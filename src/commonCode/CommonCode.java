package commonCode;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import driverPkg.TestDriver;

public class CommonCode {
	public static void storeTable(WebDriver driver,List table){
		try{
			for(int row=1;row<driver.findElements(By.xpath("//td)")).size();row++){
				String xpathExpression="//td[" + Integer.toString(row) + "]"; 
				table.add(driver.findElement(By.xpath(xpathExpression)));
			}
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println("Exception in size of list : " + e);
		}
	}
	
	public static int getTableRecordsCount(){
		int count=0;
		try{
			
			String countString="";
			String [] parsedString;
			countString=TestDriver.driver.findElement(By.cssSelector("span.k-pager-info.k-label")).getText();
			parsedString=countString.split(" ");
			count=Integer.valueOf(parsedString[parsedString.length-1]);
			
		}
		catch(ArithmeticException e){
			System.out.println("Exception in size of list : " + e);
		}
		
		return count;
	}
	
	public static void navigateToScreen(WebDriver driver,String screen){
		
	}
}
