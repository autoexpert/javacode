package configuration;

import java.util.concurrent.TimeUnit;

import logAndReport.Log4JLogging;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import driverPkg.TestDriver;

public class config {
	public static void performConfig(){
		Log4JLogging.setLogger();
		
		configureBrowserDriver(TestDriver.browser);
		Log4JLogging.info("Browser Configured");
		
		navigateToUrl(TestDriver.url);
		Log4JLogging.info("Navigattion successful");
		
		initiateWaits(TestDriver.tm);
		Log4JLogging.info("Waits Configured");
	}
	
	public static void configureBrowserDriver(String browser){
		if(browser.equalsIgnoreCase("Firefox")){
			TestDriver.driver=new FirefoxDriver();
		}
		else if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			TestDriver.driver=new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("IE")){
			System.setProperty("webdriver.ie.driver","D://Distributable/IEDriverServer.exe");
			TestDriver.driver = new InternetExplorerDriver();
		}
		TestDriver.driver.manage().window().maximize();
	}
	
	public static void navigateToUrl(String url){
		TestDriver.driver.get(url);
	}
	
	public static void initiateWaits(int tm){
		TestDriver.driver.manage().timeouts().implicitlyWait(tm, TimeUnit.SECONDS);
	}
}
