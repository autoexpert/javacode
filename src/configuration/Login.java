package configuration;

import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import logAndReport.Log4JLogging;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.examples.HSSFReadWrite;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.hamcrest.core.IsNull;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.google.common.collect.Table.Cell;

public class Login {
	//Method to perform Login
	public static void performLogin(WebDriver driver,String username,String password){
		
		
		
		enterUserName(driver,username);
		enterPassword(driver,password);
		clickSubmit(driver);
		pinMenu(driver);
		Log4JLogging.info("Login Successful");
		
	}
	
	public static void enterUserName(WebDriver driver,String username){
		driver.findElement(By.id("UserName")).sendKeys(username);
	}
	
	public static void enterPassword(WebDriver driver,String password){
		driver.findElement(By.id("Password")).sendKeys(password);
	}
	
	public static void clickSubmit(WebDriver driver){
		driver.findElement(By.className("btn")).click();
	}
	
	public static void pinMenu(WebDriver driver){
		WebDriverWait wait = new WebDriverWait(driver, 180);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("fMenuShowParentItemList")));
		driver.findElement(By.id("fMenuShowParentItemList")).click();
		WebDriverWait wait1 = new WebDriverWait(driver, 30);
		WebElement element1 = wait1.until(ExpectedConditions.elementToBeClickable(By.id("nav-pin")));
		driver.findElement(By.id("nav-pin")).click();
	}
	
	public static void logout(WebDriver driver){
		driver.quit();
	}
}
