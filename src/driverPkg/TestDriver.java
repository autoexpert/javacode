package driverPkg;

import java.io.*;
import java.util.List;
import java.util.Properties;

import logAndReport.Log4JLogging;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.WebDriver;

import commonCode.CommonCode;

import sun.security.krb5.Config;
import configuration.Login;
import configuration.config;



public class TestDriver {
	public static String browser="Firefox";
	public static WebDriver driver=null;
	public static String username="hmt";
	public static String password="hmt";
	public static String url="http://rorqa1.apptoyou.com/AWQA1/Menu/Menu";
	public static int tm=240;
	public static boolean logging=true;
	public static boolean executionResult=true;
	public static boolean screenShotOnFail=true;
	public static String layout="";
	public static String pattern="";
	public static String level="";
	public static String appender="";
	public static Logger loggr=null;
	public static List<String> internalCompanyList;
	public static Properties internalCompanyProp;
	public static int DimensionCount;
	
	public static void main(String[] args){
		try{
			//Configuration Start
			config.performConfig();
			
			//Login
			Login.performLogin(driver, username, password);
			
			System.out.println("Execution will Start");

			//Start Execution
			int success=drive();
			
			//Logout
			Login.logout(driver);
			
			System.out.println("Execution complete");

			//Poll SCM H/5 * * * *
		}
		catch(Exception e){
			System.out.println("Execution Halted!!");
		}
	}
	
	public static int drive(){
		int rows=1000;
		String keyword="",screen="";
		Log4JLogging.info("TEST STARTED");
		for(int rw=1;rw<rows;rw++){
			
			CommonCode.navigateToScreen(driver,screen);
			
			if(!keyword.equalsIgnoreCase("")){
				switch(keyword){
					case "InsertRecord":
						break;
					case "DeleteRecord":
						break;
					case "CheckLength":
						break;
					case "CheckNumeric":
						break;
					case "CheckAlpahnumeric":
						break;
					default:
						System.out.println("Execution Complete!");
						break;
						
				}
			}
			
		}
		Log4JLogging.info("TEST END");
		return 0;
	}
	
	public static String readExcel(String Path,String Sheet,int row,int col){
		String cellvalue="";
		try{
			FileInputStream fin=new FileInputStream(Path);
			HSSFWorkbook wb=new HSSFWorkbook(fin);
			HSSFSheet ws=wb.getSheet(Sheet);
			cellvalue=ws.getRow(row).getCell(col).getStringCellValue();
		}
		catch(IOException e){
			return cellvalue;
		}
		return cellvalue;
		
	}
	
	public static void writeinExcel(Workbook wb,Sheet sheet,int rw,int col,String val) throws IOException{
		  	Row row = sheet.createRow((short)rw);
		    String[] arvals=val.split(",");
		    for(int i=0; i<arvals.length;i++){
		    	row.createCell(i).setCellValue(arvals[i].toString());
		    }	        
	}
	
	
	
}
