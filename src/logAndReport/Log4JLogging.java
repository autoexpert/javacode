package logAndReport;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import driverPkg.TestDriver;



public class Log4JLogging {
	 
	 public static void setLogger(){
		 TestDriver.loggr=Logger.getLogger(Log4JLogging.class.getName());
		 PropertyConfigurator.configure("log4j.properties");
	 }
	 
	 public static void debug(String msg){
		 TestDriver.loggr.debug(msg);
	 }
	 
	 public static void info(String msg){
		 TestDriver.loggr.info(msg);
	 }
	 
	 public static void warn(String msg){
		 TestDriver.loggr.warn(msg);
	 }
	 
	 public static void error(String msg){
		 TestDriver.loggr.error(msg);
	 }
	 
	 public static void fatal(String msg){
		 TestDriver.loggr.fatal(msg);
	 }
	 
	 public static void setLevel(String level){
		 if(level.equalsIgnoreCase("DEBUG")){
			 
		 }
		 else if(level.equalsIgnoreCase("INFO")){
			 
		 }
		 else if(level.equalsIgnoreCase("WARN")){
			 
		 }
		 else if(level.equalsIgnoreCase("ERROR")){
			 
		 }
		 else if(level.equalsIgnoreCase("FATAL")){
			 
		 }			 
	 }
	 
	 public static void setLevel(int level){
		 if(level==0){
			 
		 }
		 else if(level==1){
			 
		 }
		 else if(level==2){
			 
		 }
		 else if(level==3){
			 
		 }
		 else if(level==4){
			 
		 }			 
	 }
	 
}
